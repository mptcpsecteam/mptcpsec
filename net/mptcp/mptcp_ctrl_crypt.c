/*
 *	MPTCP implementation - MPTCP-control
 *
 *	Initial Design & Implementation:
 *      Mathieu Jadin <mathieu.jadin@student.uclouvain.be> 
 *	Gautier Tihon <gautier.tihon@student.uclouvain.be>
 *
 *	This program is free software; you can redistribute it and/or
 *      modify it under the terms of the GNU General Public License
 *      as published by the Free Software Foundation; either version
 *      2 of the License, or (at your option) any later version.
 */

#include <net/mptcp.h>
#include <net/tcp.h>
#include <linux/crypto.h>

#include <linux/compiler.h>

static struct aead_spec mptcp_sec_aead_table[] = {
	{
		.name = "AEAD_AES_128_GCM",
		.algorithm_name = "gcm(aes)",
		.identifier = MPTCP_SEC_AEAD_AES_128_GCM,
		.key_length = 16,
		.tag_length = 16,
		.cipher_alignment = 1,
	},
	{
		.name = "AEAD_AES_256_GCM",
		.algorithm_name = "gcm(aes)",
		.identifier = MPTCP_SEC_AEAD_AES_256_GCM,
		.key_length = 32,
		.tag_length = 16,
		.cipher_alignment = 1,
	},
	{ }
};

struct aead_spec *mptcpsec_get_aead_spec_from_identifier(u8 identifier) {

	int i;
	for(i=0; !(mptcp_sec_aead_table[i].name) && mptcp_sec_aead_table[i].identifier != identifier; i++);
	if (mptcp_sec_aead_table[i].name) {
		return mptcp_sec_aead_table + i;
	}
	else {
        	mptcp_debug("%s : 404 Not Found !\n", __func__);
		return NULL;
	}
}

static void init_aead_worker(struct work_struct *work) {

	struct mptcp_cb *mpcb = container_of(work, struct mptcp_cb,
					     init_aead_worker);
	struct crypto_aead *send_tfm = NULL;
	struct crypto_aead *rcv_tfm = NULL;
	int err = 0;
	int mss_now = 0;


	if (sock_flag(mpcb->meta_sk, SOCK_DEAD))
	        return;


        lock_sock(mpcb->meta_sk);

	send_tfm = crypto_alloc_aead(mpcb->aead_algorithm->algorithm_name, 0, 0);
	if (IS_ERR(send_tfm)) {
		mptcp_debug("%s : Allocation of the AEAD tfm structure failed with error code %ld\n", __func__, PTR_ERR(send_tfm));
		send_tfm = NULL;
		err = PTR_ERR(send_tfm);
	        goto error;
	}
	
	rcv_tfm = crypto_alloc_aead(mpcb->aead_algorithm->algorithm_name, 0, 0);
	if (IS_ERR(rcv_tfm)) {
		mptcp_debug("%s : Allocation of the AEAD tfm structure failed with error code %ld\n", __func__, PTR_ERR(rcv_tfm));
		err = PTR_ERR(rcv_tfm);
	        rcv_tfm = NULL;
	        goto error;
	}
	
	mpcb->send_tfm = send_tfm;
	mpcb->rcv_tfm = rcv_tfm;

	err = crypto_aead_setauthsize(mpcb->send_tfm, mpcb->aead_algorithm->tag_length);
	if (err) {
		mptcp_debug("%s : Setting tag size failed with error code %d\n", __func__, err);
	        goto error;
	}
	
	err = crypto_aead_setauthsize(mpcb->rcv_tfm, mpcb->aead_algorithm->tag_length);
	if (err) {
		mptcp_debug("%s : Setting tag size failed with error code %d\n", __func__, err);
	        goto error;
	}

	mpcb->aead_send_key = kzalloc(mpcb->aead_algorithm->key_length, GFP_KERNEL);
	err = crypto_aead_setkey(mpcb->send_tfm, mpcb->aead_send_key, mpcb->aead_algorithm->key_length);
	if (err) {
		mptcp_debug("%s : Setting sender's key failed with error code %d\n", __func__, err);
		mpcb->aead_send_key = NULL;
	        goto error;
	}
	
	mpcb->aead_rcv_key = kzalloc(mpcb->aead_algorithm->key_length, GFP_KERNEL);
	err = crypto_aead_setkey(mpcb->rcv_tfm, mpcb->aead_rcv_key, mpcb->aead_algorithm->key_length);
	if (err) {
		mptcp_debug("%s : Setting receiver's key failed with error code %d\n", __func__, err);
		mpcb->aead_rcv_key = NULL;
	        goto error;
	}

	/* Start encryption of already penging segments */
	mss_now = mptcp_current_mss(mpcb->meta_sk);
	mptcpsec_data_encr_until(mpcb->meta_sk, NULL, tcp_sk(mpcb->meta_sk)->nonagle, mss_now);

	/* Start authentication and decryption of all received segments */
	mptcpsec_data_check_auth_all(mpcb->meta_sk);

	release_sock(mpcb->meta_sk);

	return;
error:
	if (send_tfm) {
	        crypto_free_aead(send_tfm);
		mpcb->send_tfm = NULL;
	}
	if (rcv_tfm) {
		crypto_free_aead(rcv_tfm);
		mpcb->rcv_tfm = NULL;
	}
	if (mpcb->aead_send_key) {
		kfree(mpcb->aead_send_key);
		mpcb->aead_send_key = NULL;
	}
	if (mpcb->aead_rcv_key) {
		kfree(mpcb->aead_rcv_key);
		mpcb->aead_rcv_key = NULL;
	}
        release_sock(mpcb->meta_sk);
}

/**
 * Schedule a work to initialize crypto structures
 */
int mptcpsec_init_aead(struct mptcp_cb *mpcb, int aead_identifier) {

	mpcb->aead_algorithm = mptcpsec_get_aead_spec_from_identifier(aead_identifier);
	if (!mpcb->aead_algorithm) {
		mptcp_debug("%s : Algo not found : %p\n", __func__, mpcb->aead_algorithm);
	        return 1;
	}
	
	INIT_WORK(&mpcb->init_aead_worker, init_aead_worker);
	queue_work(mptcpsec_wq, &mpcb->init_aead_worker);
	return 0;
}

/* Pruning related functions */

void mptcpsec_purge_to_encrypt_queue(struct sock *meta_sk) {

	skb_queue_purge(&tcp_sk(meta_sk)->mpcb->mptcpsec_to_encrypt_queue);
}

void mptcpsec_purge_encrypted_ofo_queue(struct sock *meta_sk) {

	skb_queue_purge(&tcp_sk(meta_sk)->mpcb->mptcpsec_encrypted_out_of_order_queue);
}

void mptcpsec_purge_to_decrypt_queue(struct sock *meta_sk) {

	skb_queue_purge(&tcp_sk(meta_sk)->mpcb->mptcpsec_to_decrypt_queue);
}

void mptcpsec_purge_to_decrypt_ofo_queue(struct sock *meta_sk) {

	skb_queue_purge(&tcp_sk(meta_sk)->mpcb->mptcpsec_to_decrypt_out_of_order_queue);
}

/* AEAD failure related functions */

void mptcpsec_advance_decrypt_nxt(struct sock *sk, struct sk_buff *new_skb) {

	struct tcp_sock *meta_tp = tcp_sk(tcp_sk(sk)->meta_sk);
	struct mptcp_cb *mpcb = meta_tp->mpcb;

	struct sk_buff *cur_skb = NULL;
	struct sk_buff *next_skb = NULL;

	mpcb->decrypt_nxt = TCP_SKB_CB(new_skb)->end_seq;

	/* Since encryption and authentication is launched both on the decrypt queue 
	 * and on the decrypt ofo queue, it could be that decrypt_nxt should increase
	 * more than the number of bytes inside this segment.
	 */
        skb_queue_walk_safe(&meta_tp->out_of_order_queue, cur_skb, next_skb) {

		if (after(mpcb->decrypt_nxt, TCP_SKB_CB(cur_skb)->seq)) {
			continue;
		} else if (mpcb->decrypt_nxt == TCP_SKB_CB(cur_skb)->seq) {
			mpcb->decrypt_nxt = TCP_SKB_CB(cur_skb)->end_seq;
		} else {
			break;
		}
	}
}

static void mptcpsec_decrease_decrypt_nxt(struct sock *sk, struct sk_buff *wrong_skb) {

	struct tcp_sock *meta_tp = tcp_sk(tcp_sk(sk)->meta_sk);
	struct mptcp_cb *mpcb = meta_tp->mpcb;

	struct sk_buff *cur_skb = NULL;
	struct sk_buff *previous_skb = NULL;

	mpcb->decrypt_nxt = TCP_SKB_CB(wrong_skb)->seq;

	/* Segments that were in sequence are now out-of-sequence */
        skb_queue_reverse_walk_safe(&mpcb->mptcpsec_to_decrypt_queue, cur_skb, previous_skb) {

		/* Unlink from the tail of the decrypt queue */
	        __skb_unlink(cur_skb, &mpcb->mptcpsec_to_decrypt_queue);

		/* Add it to the front of the ofo decrypt queue */
		__skb_queue_head(&mpcb->mptcpsec_to_decrypt_out_of_order_queue, cur_skb);

		if (previous_skb == wrong_skb) {
			break;
		}
	}

	__skb_unlink(wrong_skb, &mpcb->mptcpsec_to_decrypt_queue);
}

static void mptcpsec_reject_skb(struct sock *sk, struct sk_buff *skb) {

	struct sock *cur_sk = NULL;
	struct sock *next_sk = NULL;

	struct mptcp_cb *mpcb = tcp_sk(sk)->mpcb;
	
	/* Change decrypt state and queues */
	mptcpsec_decrease_decrypt_nxt(sk, skb);

	/* Close subflow if still in the connection list */
	mptcp_for_each_sk_safe(mpcb, cur_sk, next_sk) {

		if (cur_sk == sk) {
			mptcp_send_reset(sk);
		}
	}
	
	/* Free skb */
	__kfree_skb(skb);
}

static void mptcpsec_reject_skb_lock(struct sock *sk, struct sk_buff *skb) {

	struct sock *meta_sk = tcp_sk(sk)->meta_sk;
	
	bh_lock_sock_nested(meta_sk);
	mptcpsec_reject_skb(sk, skb);
	bh_unlock_sock(meta_sk);
}

/* Scatter-list related functions */

static int get_number_fragments(struct sk_buff *skb, u32 start_offset, u32 end_offset,
			        int *start_frag, int *end_frag, int *start_frag_offset,
				int *end_frag_offset) {

	struct skb_shared_info *skb_sh = skb_shinfo(skb);
	struct skb_frag_struct *cur_frag;
	int i;

	if (start_offset > end_offset) {
		return -1;
	}
	if (!skb_sh->nr_frags) {
		return 0;
	}
	
	*start_frag = -1;
	*end_frag = -1;
	for (i = 0; i < skb_sh->nr_frags; i++) {
		cur_frag = &skb_sh->frags[i];
		if (*start_frag < 0 && start_offset < cur_frag->size) {
			*start_frag = i;
			*start_frag_offset = start_offset;
		}
		if (*end_frag < 0 && end_offset < cur_frag->size) {
			*end_frag = i;
			*end_frag_offset = end_offset;
		}
		if (*start_frag < 0) {
			start_offset -= cur_frag->size;
		}
		if (*end_frag < 0) {
			end_offset -= cur_frag->size;
		}
	}

	if (start_frag < 0 || end_frag < 0) {
		return -1;
	}

	return *end_frag - *start_frag + 1;
}

static void set_sg_table_from_skb_frags(struct sk_buff *skb, u32 start_frag, u32 end_frag,
					u32 start_frag_offset, u32 end_frag_offset,
					struct scatterlist *list) {

	struct skb_shared_info *skb_sh = skb_shinfo(skb);
	struct skb_frag_struct *frags = skb_sh->frags;
	struct skb_frag_struct *current_frag;
	u32 nents = end_frag - start_frag + 1;
	int i;

	/* Set the table */
	sg_init_table(list, nents);

	if (nents == 1) {
		current_frag = &frags[start_frag];
		sg_set_page(list, current_frag->page.p,
			    (end_frag_offset + 1) - start_frag_offset,
			    current_frag->page_offset + start_frag_offset);
		return;
	}

	/* Start page */
	current_frag = frags;
	sg_set_page(list, current_frag->page.p, current_frag->size - start_frag_offset,
		    current_frag->page_offset + start_frag_offset);

	/* Set middle sg_pages */
	for(i = 1; i < nents - 1; i++) {
	        current_frag = &frags[start_frag + i];
		sg_set_page(&list[i], current_frag->page.p, current_frag->size,
			    current_frag->page_offset);
	}

	/* End page */
	current_frag = &frags[end_frag];
	sg_set_page(&list[nents-1], current_frag->page.p, end_frag_offset + 1,
		    current_frag->page_offset);
}

/* Encryption related functions */

void mptcpsec_encrypt_queue_entail(struct sock *sk, struct sk_buff *skb) {

	struct tcp_sock *tp = tcp_sk(sk);
	struct tcp_skb_cb *tcb = TCP_SKB_CB(skb);

	skb->csum    = 0;
	tcb->seq     = tcb->end_seq = tp->mpcb->encrypt_seq;
	tcb->tcp_flags = TCPHDR_ACK;
	tcb->sacked  = 0;
	__skb_header_release(skb);
	mptcpsec_add_encrypt_queue_tail(sk, skb);
	sk->sk_wmem_queued += skb->truesize;
	sk_mem_charge(sk, skb->truesize);
	if (tp->nonagle & TCP_NAGLE_PUSH)
		tp->nonagle &= ~TCP_NAGLE_PUSH;
}

static inline void mptcpsec_write_queue_entail(struct sock *meta_sk,
					       struct sk_buff *skb) {

	mptcpsec_unlink_encrypt_queue(skb, meta_sk);
	mptcpsec_check_encrypt_head(meta_sk, skb);

	__tcp_add_write_queue_tail(meta_sk, skb);
	tcp_sk(meta_sk)->write_seq = TCP_SKB_CB(skb)->end_seq;

	/* Queue it, remembering where we must start sending. */
	if (meta_sk->sk_send_head == NULL) {
		meta_sk->sk_send_head = skb;
	}
}

static bool mptcpsec_empty_encrypted_ofo_queue(struct sock *meta_sk) {

	struct sk_buff *cur_skb = NULL;
	struct sk_buff *next_skb = NULL;
	struct mptcp_cb *mpcb = tcp_sk(meta_sk)->mpcb;
	bool saw_data_fin = false;
	
	skb_queue_walk_safe(&mpcb->mptcpsec_encrypted_out_of_order_queue,
			    cur_skb, next_skb) {
		if (tcp_sk(meta_sk)->write_seq == TCP_SKB_CB(cur_skb)->seq) {

		        if (TCP_SKB_CB(cur_skb)->mptcp_flags & MPTCPHDR_FIN) {
				/* The Data-Fin segment will be queued on meta write queue */
				saw_data_fin = true;
			}
			
			__skb_unlink(cur_skb, &mpcb->mptcpsec_encrypted_out_of_order_queue);
			
			__tcp_add_write_queue_tail(meta_sk, cur_skb);
			tcp_sk(meta_sk)->write_seq = TCP_SKB_CB(cur_skb)->end_seq;

			/* Queue it, remembering where we must start sending. */
			if (meta_sk->sk_send_head == NULL) {
				meta_sk->sk_send_head = cur_skb;
			}
		} else {
			break;
		}
	}
	return saw_data_fin;
}

static void mptcpsec_encrypted_ofo_queue_add_skb(struct sock *meta_sk, struct sk_buff *skb) {

	struct sk_buff *cur_skb = NULL;
	struct sk_buff *next_skb = NULL;
	struct mptcp_cb *mpcb = tcp_sk(meta_sk)->mpcb;
	struct tcp_skb_cb *tcb = TCP_SKB_CB(skb);
	
        skb_queue_walk_safe(&mpcb->mptcpsec_encrypted_out_of_order_queue,
			    cur_skb, next_skb) {
		struct tcp_skb_cb *cur_tcb =  TCP_SKB_CB(cur_skb);
		
	        if (after(cur_tcb->seq, tcb->seq)) {
			
			mptcpsec_unlink_encrypt_queue(skb, meta_sk);
			mptcpsec_check_encrypt_head(meta_sk, skb);
			
		        __skb_queue_before(&mpcb->mptcpsec_encrypted_out_of_order_queue,
					   cur_skb, skb);
			return;
		}
	}

        /* Put at the end if not feasible before */
	mptcpsec_unlink_encrypt_queue(skb, meta_sk);
        __skb_queue_tail(&mpcb->mptcpsec_encrypted_out_of_order_queue, skb);
}

void mptcpsec_send_fin(struct sock *meta_sk) {

	struct tcp_sock *meta_tp = tcp_sk(meta_sk);
	struct mptcp_cb *mpcb = meta_tp->mpcb;
	struct sk_buff *skb = NULL;
	struct sk_buff *skb_tmp = NULL;
	int mss_now;

	if ((1 << meta_sk->sk_state) & (TCPF_CLOSE_WAIT | TCPF_LAST_ACK))
		meta_tp->mpcb->passive_close = 1;

	/* Optimization, tack on the FIN if we have a queue of
	 * unsent frames.  But be careful about outgoing SACKS
	 * and IP options.
	 */
	mss_now = mptcp_current_mss(meta_sk);

	/* Find where to add the data-fin message */
	skb = mptcpsec_encrypt_queue_tail(meta_sk);
	if (skb == NULL) {
		/* All was already encrypted and should be in meta write queue
		 * (Indeed, as everything is sent and encrypted, nothing should be
		 * on the Encrypt out-of-order queue)
		 */
	        if (tcp_send_head(meta_sk) != NULL) {
			skb = skb_peek_tail(&meta_sk->sk_write_queue);
			TCP_SKB_CB(skb)->mptcp_flags |= MPTCPHDR_FIN;
			TCP_SKB_CB(skb)->end_seq++;
			meta_tp->write_seq++;
			mpcb->encrypt_seq++;
		} else {
			/* Socket is locked, keep trying until memory is available. */
			for (;;) {
				skb = alloc_skb_fclone(MAX_TCP_HEADER,
						       meta_sk->sk_allocation);
				if (skb)
					break;
				yield();
			}
			/* Reserve space for headers and prepare control bits. */
			skb_reserve(skb, MAX_TCP_HEADER);

			tcp_init_nondata_skb(skb, meta_tp->write_seq, TCPHDR_ACK);
			TCP_SKB_CB(skb)->end_seq++;
			TCP_SKB_CB(skb)->mptcp_flags |= MPTCPHDR_FIN;
			mpcb->encrypt_seq = TCP_SKB_CB(skb)->end_seq;
			tcp_queue_skb(meta_sk, skb);
		}
		__tcp_push_pending_frames(meta_sk, mss_now, TCP_NAGLE_OFF);
	} else {
		
		/* The last segment could be already encrypted but not in order */
		skb_tmp = skb_peek_tail(&mpcb->mptcpsec_encrypted_out_of_order_queue);
		if (skb_tmp && TCP_SKB_CB(skb)->end_seq < TCP_SKB_CB(skb_tmp)->end_seq)
			skb = skb_tmp;
		
		/* No need for an additional segment
		 * since options are not yet authenticated
		 */
		TCP_SKB_CB(skb)->mptcp_flags |= MPTCPHDR_FIN;
	        TCP_SKB_CB(skb)->end_seq++;
	        mpcb->encrypt_seq++;
		/* Launch encryption of the remaining segments */
		if (mpcb->encrypt_head != NULL)
			mptcpsec_data_encr_until(meta_sk, NULL, mss_now, TCP_NAGLE_OFF);
	}
}

static inline void mptcpsec_push_write_queue(struct sock *meta_sk, struct sk_buff *skb,
					     u8 push_one, int nonagle, int mss_now,
					     bool saw_data_fin) {

	if (saw_data_fin) {
		tcp_sk(meta_sk)->ops->write_xmit(meta_sk, mss_now, TCP_NAGLE_OFF, 0,
						 meta_sk->sk_allocation);
	} else if (tcp_skb_is_last(meta_sk, skb) && push_one) {
	        tcp_sk(meta_sk)->ops->write_xmit(meta_sk, mss_now, nonagle, 1,
						 meta_sk->sk_allocation);
	} else {
		if (tcp_sk(meta_sk)->ops->write_xmit(meta_sk, mss_now, nonagle, 0,
						     sk_gfp_atomic(meta_sk, GFP_ATOMIC)))
			tcp_check_probe_timer(meta_sk);
	}
}

int mptcpsec_data_encr_until(struct sock *sk, struct sk_buff *skb, int mss_now, int nonagle) {
	
	struct sk_buff *cur_skb = mptcpsec_encrypt_head(sk);
	struct sk_buff *next_skb = NULL;
	int err = 0;
	struct mptcp_cb *mpcb = tcp_sk(sk)->mpcb;

	if (!cur_skb) /* Nothing to do */
		return 0;
	
        skb_queue_walk_from_safe(&mpcb->mptcpsec_to_encrypt_queue, cur_skb, next_skb) {
		err = mptcpsec_data_encr(sk, cur_skb, false, mss_now, nonagle);
		if (err)
			return err;
		if (cur_skb == skb)
			return 0;
	}
	return 0;
}

static void mptcpsec_data_encr_finished(struct sock *meta_sk, struct sk_buff *skb,
					u8 push_one, int nonagle, int mss_now) {

	skb->csum = 0; /* Because it could have been computed on the unencrypted data */
	skb_put(skb, mptcpsec_tags_space(meta_sk)); /* Count data & option tags */
	
	/* Try to add it directly to meta write queue if possible */
	if (tcp_sk(meta_sk)->write_seq == TCP_SKB_CB(skb)->seq) {
		bool saw_data_fin = TCP_SKB_CB(skb)->mptcp_flags & MPTCPHDR_FIN;
		mptcpsec_write_queue_entail(meta_sk, skb);
		saw_data_fin |= mptcpsec_empty_encrypted_ofo_queue(meta_sk);
	        mptcpsec_push_write_queue(meta_sk, skb, push_one, nonagle, mss_now,
					  saw_data_fin);
	}
	else
		mptcpsec_encrypted_ofo_queue_add_skb(meta_sk, skb);
}

void mptcpsec_data_encr_callback(struct crypto_async_request *req, int err) {

        struct mptcpsec_encr_callback_args *args =
		(struct mptcpsec_encr_callback_args *) req->data;
	struct sk_buff *skb = args->skb;
	struct sock *meta_sk = args->meta_sk;
	u8 push_one = args->push_one;
	int nonagle = args->nonagle;
	int mss_now = args->mss_now;
	__be64 *data_seq = args->data_seq;
	struct scatterlist *scatter_sg = args->scatter_sg;

	if (err == -EINPROGRESS) {
		/* The request was backlogged => this function will be called again later
		 * when the encryption will be finished (or if an error was raised)
		 */
		return;
	}

	if (err) {
		mptcp_debug("%s : The encryption has failed to process with error code %d\n", __func__, err);
		goto free_structures;
	}

	if (unlikely(sock_flag(meta_sk, SOCK_DEAD))) {
	        mptcp_debug("%s : The sock died before the encryption was finished\n", __func__);
		goto free_structures;
	}

        /* If we are closed, the bytes will have to remain here.
         * In time closedown will finish, we empty the write queue and
         * all will be happy.
         */
	if (unlikely(meta_sk->sk_state == TCP_CLOSE)) {
		goto free_structures;
	}

        /* Since we come from a new thread, we must be lock the meta sock */
	bh_lock_sock_nested(meta_sk);

	/* Copy ciphertext to the skb and change its queue */
        mptcpsec_data_encr_finished(meta_sk, skb, push_one, nonagle, mss_now);

	bh_unlock_sock(meta_sk);

free_structures:
	/* Free structures */
	kfree(args);
	req->data = NULL;
	kfree(data_seq);
	kfree(scatter_sg);
	aead_request_free((struct aead_request *) req);
}

int mptcpsec_data_encr(struct sock *sk, struct sk_buff *skb, bool push_one,
		       int mss_now, int nonagle) {
	
	int err = 0;
	
	struct scatterlist *src_sg = (struct scatterlist *)
		kzalloc(3 * sizeof(struct scatterlist), GFP_KERNEL);
	struct scatterlist *dst_sg = src_sg + 1;
	struct scatterlist *aad_sg = dst_sg + 1;
	
	struct mptcp_cb *mpcb = tcp_sk(sk)->mpcb;
	__be64 *data_seq =  (__be64 *) kzalloc(3*sizeof(__u64), GFP_KERNEL);
	__u64 *data_level_length = data_seq+2;
	u8 *nonce = (u8 *) data_seq;

	struct crypto_aead *tfm = tcp_sk(sk)->mpcb->send_tfm;
	struct aead_request *req = NULL;
	struct mptcpsec_encr_callback_args *encryption_arguments = NULL;
	
	*data_seq = mptcpsec_get_encrypt_data_seq_64(skb, mpcb);
	*data_level_length = mptcpsec_data_cipher_space(sk, skb);

	if (!tcp_sk(sk)->mpcb->send_tfm) {
		mptcp_debug("%s : Structure not yet initialized\n", __func__);
		err = 1;
		goto free_structures;
	} else if (!skb->len) {
	        mptcpsec_advance_encrypt_head(sk, skb);
	        mptcpsec_data_encr_finished(sk, skb, push_one, nonagle, mss_now);
		goto free_structures;
	}

        req = aead_request_alloc(tfm, GFP_KERNEL);
	if (IS_ERR(req)) {
	        err = PTR_ERR(req);
		goto free_structures;
	}

	/* Allocation */
	sg_init_one(src_sg, skb->data, skb->len);
	sg_init_one(dst_sg, skb->data, mptcpsec_data_cipher_space(sk, skb));
	sg_init_one(aad_sg, data_level_length, 8);

	/* Request fill-in */
	encryption_arguments = (struct mptcpsec_encr_callback_args *)
		kzalloc(sizeof(struct mptcpsec_encr_callback_args), GFP_KERNEL);
	if (!encryption_arguments) {
		err = 1;
		goto free_structures;
	}
	encryption_arguments->meta_sk = sk;
	encryption_arguments->skb = skb;
	encryption_arguments->push_one = push_one;
	encryption_arguments->nonagle = nonagle;
	encryption_arguments->mss_now = mss_now;
	encryption_arguments->data_seq = data_seq;
	encryption_arguments->scatter_sg = src_sg;
	
	aead_request_set_callback(req, CRYPTO_TFM_REQ_MAY_BACKLOG, mptcpsec_data_encr_callback, (void *) encryption_arguments);
	aead_request_set_crypt(req, src_sg, dst_sg, skb->len, nonce);
	aead_request_set_assoc(req, NULL, 0);

	/* Launch aead & wait for completion */
        err = crypto_aead_encrypt(req);
	if (err == -EINPROGRESS || err == -EBUSY) {
        	mptcpsec_advance_encrypt_head(sk, skb);
		return err;
	} else if (err < 0) {
		mptcp_debug("%s : The encryption has failed to start with error code %d\n", __func__, err);
	} else {
		/* Encryption is finished :
		 * 	- advance encrypt head
		 * 	- change the queue of the skb
		 */
		mptcpsec_advance_encrypt_head(sk, skb);
	        mptcpsec_data_encr_finished(sk, skb, push_one, nonagle, mss_now);
	}

free_structures:
	/* Free structures */
	if (req)
		aead_request_free(req);
	if (data_seq)
		kfree(data_seq); /* Free also the data_level_length */
	if (src_sg)
		kfree(src_sg); /* Free also the dst_sg and aad_sg */
	if (encryption_arguments)
		kfree(encryption_arguments);
	return err;
}

/* Decryption related functions */

/**
 * Does 'skb' fits after 'here' in the queue 'head' ?
 * If yes, we queue it and return 1
 * (inspired from mptcp_ofo_queue_after)
 */
static int mptcpsec_ofo_queue_after(struct sk_buff_head *head,
				    struct sk_buff *skb, struct sk_buff *here)
{
	u32 seq = TCP_SKB_CB(skb)->seq;
	u32 end_seq = TCP_SKB_CB(skb)->end_seq;

	/* We want to queue skb after here, thus seq >= end_seq */
	if (before(seq, TCP_SKB_CB(here)->end_seq))
		return 0;

	if (seq == TCP_SKB_CB(here)->end_seq) {
		 __skb_queue_after(head, here, skb);
		return 1;
	}

	/* If here is the last one, we can always queue it */
	if (skb_queue_is_last(head, here)) {
		__skb_queue_after(head, here, skb);
		return 1;
	} else {
		struct sk_buff *skb1 = skb_queue_next(head, here);
		/* It's not the last one, but does it fits between 'here' and
		 * the one after 'here' ? Thus, does end_seq <= after_here->seq
		 */
		if (!after(end_seq, TCP_SKB_CB(skb1)->seq)) {
			__skb_queue_after(head, here, skb);
			return 1;
		}
	}

	return 0;
}

static void mptcpsec_try_shortcut(struct sk_buff *shortcut, struct sk_buff *skb,
				  struct sk_buff_head *head, struct tcp_sock *tp,
				  bool no_shortcut) {

	struct sock *meta_sk = tp->meta_sk;
	struct tcp_sock *tp_it, *meta_tp = tcp_sk(meta_sk);
	struct mptcp_cb *mpcb = meta_tp->mpcb;
	struct sk_buff *skb1, *best_shortcut = NULL;
	u32 seq = TCP_SKB_CB(skb)->seq;
	u32 end_seq = TCP_SKB_CB(skb)->end_seq;
	u32 distance = 0xffffffff;

	/* First, check the tp's shortcut */
	if (!shortcut) {
		if (skb_queue_empty(head)) {
			__skb_queue_head(head, skb);
			goto end;
		}
	} else {
		int ret = mptcpsec_ofo_queue_after(head, skb, shortcut);
		/* Does the tp's shortcut is a hit? If yes, we insert. */

		if (ret) {
			skb = (ret > 0) ? skb : NULL;
			goto end;
		}
	}

	if (no_shortcut)
		goto no_shortcut;

	/* Check the shortcuts of the other subsockets. */
	mptcp_for_each_tp(mpcb, tp_it) {
		shortcut = tp_it->mptcp->shortcut_ofoqueue;
		/* Can we queue it here? If yes, do so! */
		if (shortcut) {
			int ret = mptcpsec_ofo_queue_after(head, skb, shortcut);

			if (ret) {
				skb = (ret > 0) ? skb : NULL;
				goto end;
			}
		}

		/* Could not queue it, check if we are close.
		 * We are looking for a shortcut, close enough to seq to
		 * set skb1 prematurely and thus improve the subsequent lookup,
		 * which tries to find a skb1 so that skb1->seq <= seq.
		 *
		 * So, here we only take shortcuts, whose shortcut->seq > seq,
		 * and minimize the distance between shortcut->seq and seq and
		 * set best_shortcut to this one with the minimal distance.
		 *
		 * That way, the subsequent while-loop is shortest.
		 */
		if (shortcut && after(TCP_SKB_CB(shortcut)->seq, seq)) {
			/* Are we closer than the current best shortcut? */
			if ((u32)(TCP_SKB_CB(shortcut)->seq - seq) < distance) {
				distance = (u32)(TCP_SKB_CB(shortcut)->seq - seq);
				best_shortcut = shortcut;
			}
		}
	}

no_shortcut:

	if (best_shortcut)
		skb1 = best_shortcut;
	else
		skb1 = skb_peek_tail(head);

	if (seq == TCP_SKB_CB(skb1)->end_seq) {
	        __skb_queue_after(head, skb1, skb);
		goto end;
	}

	/* Find the insertion point, starting from best_shortcut if available.
	 *
	 * Inspired from tcp_data_queue_ofo.
	 */
	while (1) {
		/* skb1->seq <= seq */
		if (!after(TCP_SKB_CB(skb1)->seq, seq))
			break;
		if (skb_queue_is_first(head, skb1)) {
			skb1 = NULL;
			break;
		}
		skb1 = skb_queue_prev(head, skb1);
	}

	/* Do skb overlap to previous one? */
	if (skb1 && before(seq, TCP_SKB_CB(skb1)->end_seq)) {
		if (!after(end_seq, TCP_SKB_CB(skb1)->end_seq)) {
			/* All the bits are present. */
			__kfree_skb(skb);
			skb = NULL;
			goto end;
		}
		if (seq == TCP_SKB_CB(skb1)->seq) {
			if (skb_queue_is_first(head, skb1))
				skb1 = NULL;
			else
				skb1 = skb_queue_prev(head, skb1);
		}
	}
	if (!skb1)
		__skb_queue_head(head, skb);
	else
		__skb_queue_after(head, skb1, skb);

	/* And clean segments covered by new one as whole. */
	while (!skb_queue_is_last(head, skb)) {
		skb1 = skb_queue_next(head, skb);

		if (!after(end_seq, TCP_SKB_CB(skb1)->seq))
			break;

		__skb_unlink(skb1, head);
		mptcp_remove_shortcuts(mpcb, skb1);
		__kfree_skb(skb1);
	}

end:
	if (!no_shortcut && skb) {
		skb_set_owner_r(skb, meta_sk);
		tp->mptcp->shortcut_ofoqueue = skb;
	}
}

/**
 * @sk: the subflow that received this skb.
 */
void mptcpsec_add_to_decrypt_ofo_queue(struct sock *sk, struct sk_buff *skb) {

	struct tcp_sock *tp = tcp_sk(sk);

	mptcpsec_try_shortcut(tp->mptcp->shortcut_ofoqueue, skb,
			      &tp->mpcb->mptcpsec_to_decrypt_out_of_order_queue, tp, false);
	mptcpsec_option_check_auth(sk, skb);
}

/**
 * @sk: the subflow that received this skb.
 */
void mptcpsec_queue_to_decrypt(struct sock *sk, struct sk_buff *skb) {

	struct sock *meta_sk = mptcp_meta_sk(sk);
	struct mptcp_cb *mpcb = tcp_sk(meta_sk)->mpcb;
        __skb_queue_tail(&mpcb->mptcpsec_to_decrypt_queue, skb);
        skb_set_owner_r(skb, mptcp_meta_sk(sk));
	mptcpsec_option_check_auth(sk, skb);
}

void mptcpsec_to_decrypt_ofo_queue(struct sock *meta_sk) {

        struct tcp_sock *meta_tp = tcp_sk(meta_sk);
	struct mptcp_cb *mpcb = meta_tp->mpcb;
	struct sk_buff *skb;

	while ((skb = skb_peek(&mpcb->mptcpsec_to_decrypt_out_of_order_queue)) != NULL) {
		if (after(TCP_SKB_CB(skb)->seq, mpcb->decrypt_nxt))
			break;

		if (!after(TCP_SKB_CB(skb)->end_seq, mpcb->decrypt_nxt)) {
			__skb_unlink(skb, &mpcb->mptcpsec_to_decrypt_out_of_order_queue);
			mptcp_remove_shortcuts(mpcb, skb);
			__kfree_skb(skb);
			continue;
		}

		__skb_unlink(skb, &mpcb->mptcpsec_to_decrypt_out_of_order_queue);
		mptcp_remove_shortcuts(mpcb, skb);
		
		__skb_queue_tail(&mpcb->mptcpsec_to_decrypt_queue, skb);
		mptcpsec_advance_decrypt_nxt(meta_sk, skb);
	}
}

/* If we update tp->rcv_nxt, also update tp->bytes_received */
static void mptcpsec_rcv_nxt_update(struct tcp_sock *tp, u32 seq)
{
	u32 delta = seq - tp->rcv_nxt;

	u64_stats_update_begin(&tp->syncp);
	tp->bytes_received += delta;
	u64_stats_update_end(&tp->syncp);
	tp->rcv_nxt = seq;
}

static void mptcpsec_receive_queue_entail(struct sock *meta_sk, struct sk_buff *skb) {

	struct tcp_sock *meta_tp = tcp_sk(meta_sk);
	u32 old_rcv_nxt = meta_tp->rcv_nxt;
	
        BUG_ON(tcp_sk(meta_sk)->rcv_nxt != TCP_SKB_CB(skb)->seq);
	
	mptcpsec_rcv_nxt_update(meta_tp, TCP_SKB_CB(skb)->end_seq);
	mptcp_check_rcvseq_wrap(meta_tp, old_rcv_nxt);

	__skb_unlink(skb, &meta_tp->mpcb->mptcpsec_to_decrypt_queue);
        __skb_queue_tail(&meta_sk->sk_receive_queue, skb);

	/* Interpreted here because all data should now be decrypted 
	 * (only if there is a subflow left)
	 */
	if (TCP_SKB_CB(skb)->tcp_flags & TCPHDR_FIN) {
		mptcp_fin(meta_sk);
	}
}

static void mptcpsec_ofo_queue(struct sock *meta_sk) {

        struct tcp_sock *meta_tp = tcp_sk(meta_sk);
	struct sk_buff *skb;

	while ((skb = skb_peek(&meta_tp->out_of_order_queue)) != NULL) {
		u32 old_rcv_nxt = meta_tp->rcv_nxt;
		if (after(TCP_SKB_CB(skb)->seq, meta_tp->rcv_nxt)) {
			break;
		}

		if (!after(TCP_SKB_CB(skb)->end_seq, meta_tp->rcv_nxt)) {
			__skb_unlink(skb, &meta_tp->out_of_order_queue);
			__kfree_skb(skb);
			continue;
		}

		__skb_unlink(skb, &meta_tp->out_of_order_queue);

		__skb_queue_tail(&meta_sk->sk_receive_queue, skb);
		meta_tp->rcv_nxt = TCP_SKB_CB(skb)->end_seq;
		mptcp_check_rcvseq_wrap(meta_tp, old_rcv_nxt);

		/* Call mptcp_fin only if there is a subflow left */
		if (TCP_SKB_CB(skb)->tcp_flags & TCPHDR_FIN)
			mptcp_fin(meta_sk);
	}
}

static void mptcpsec_ofo_queue_add_skb(struct sock *meta_sk, struct sock *subsk,
				       struct sk_buff *skb) {

	struct tcp_sock *meta_tp = tcp_sk(meta_sk);
	struct mptcp_cb *mpcb = meta_tp->mpcb;
	
	if (before(mpcb->decrypt_nxt, TCP_SKB_CB(skb)->end_seq)) {
		/* We are inside decrypt out-of-order queue */
		__skb_unlink(skb, &mpcb->mptcpsec_to_decrypt_out_of_order_queue);

		/* Don't forget to remove the potential shortcut */
	        if (tcp_sk(subsk)->mptcp->shortcut_ofoqueue == skb) {
			tcp_sk(subsk)->mptcp->shortcut_ofoqueue = NULL;
		}
	} else {
		/* We are inside decrypt queue */
		__skb_unlink(skb, &mpcb->mptcpsec_to_decrypt_queue);
	}
	
        mptcpsec_try_shortcut(NULL, skb, &meta_tp->out_of_order_queue, meta_tp, true);
}

static void mptcpsec_data_decr_entail_queue(struct sock *meta_sk, struct sock *subsk,
					    struct sk_buff *skb) {

	struct tcp_sock *meta_tp = tcp_sk(meta_sk);
	struct mptcp_cb *mpcb = meta_tp->mpcb;
	struct tcp_sock *subtp = tcp_sk(subsk);
        
	/* If we have a valid DATA-FIN - We recall the path on which it was sent (if a subflow still exists) */
	if (TCP_SKB_CB(skb)->tcp_flags & TCPHDR_FIN && mpcb->cnt_subflows) {
        	mpcb->dfin_path_index = subtp->mptcp->path_index;
		mpcb->dfin_combined = !!(subsk->sk_shutdown & RCV_SHUTDOWN);
	}

	/* Try to add it directly to meta write queue if possible */
	if (tcp_sk(meta_sk)->rcv_nxt == TCP_SKB_CB(skb)->seq) {
		
		mptcpsec_receive_queue_entail(meta_sk, skb);
	        mptcpsec_ofo_queue(meta_sk);

		/* Signal to the application that data can be read */
        	if (!sock_flag(meta_sk, SOCK_DEAD)) {
			meta_sk->sk_data_ready(meta_sk);
		}
        } else {
		mptcpsec_ofo_queue_add_skb(meta_sk, subsk, skb);
	}
}

static void mptcpsec_data_decr_finished(struct sock *meta_sk, struct sock *subsk,
					struct sk_buff *skb) {

	skb->csum = 0; /* Because it could have been computed on the encrypted data */
	
        mptcpsec_data_decr_entail_queue(meta_sk, subsk, skb);
}

void mptcpsec_data_decr_callback(struct crypto_async_request *req, int err) {

	struct mptcpsec_decr_callback_args *args =
		(struct mptcpsec_decr_callback_args *) req->data;
	struct sk_buff *skb = args->skb;
	struct sock *sk = args->subsk;
	struct sock *meta_sk = mptcp_meta_sk(sk);
	u64 *data_seq = args->data_seq;
	struct scatterlist *scatter_sg = args->scatter_sg;

	if (err == -EINPROGRESS) {
		/* The request was backlogged => this function will be called again later
		 * when the decryption will be finished (or if an error was raised)
		 */
		return;
	}

	if (unlikely(sock_flag(meta_sk, SOCK_DEAD))) {
	        mptcp_debug("%s : The meta sock died before the decryption was finished\n", __func__);
		goto free_structures;
	} else if (unlikely(meta_sk->sk_state == TCP_CLOSE || sock_flag(meta_sk, SOCK_DONE))) {
		mptcp_debug("%s : The meta sock was closed before the decryption was finished or the segment is after the DATA-FIN\n", __func__);
		mptcpsec_reject_skb_lock(sk, skb);
		goto free_structures;
	}

	if (err == -EBADMSG) {
		mptcp_debug("%s : Segment raised authentication error\n", __func__);
	        mptcpsec_reject_skb_lock(sk, skb);
		goto free_structures;
	} else if (err) {
		mptcp_debug("%s : The decryption has failed to process with error code %d\n", __func__, err);
	        mptcpsec_reject_skb_lock(sk, skb);
		goto free_structures;
	}

        /* Since we come from a new thread, we must be lock the meta sock */
	bh_lock_sock(meta_sk);

	/* Copy ciphertext to the skb and change its queue */
        mptcpsec_data_decr_finished(meta_sk, sk, skb);
	
	bh_unlock_sock(meta_sk);

free_structures:
	/* Free structures */
	kfree(args);
	kfree(data_seq);
	req->data = NULL;
	kfree(scatter_sg);
	aead_request_free((struct aead_request *) req);
}

int mptcpsec_data_decr(struct sock *sk, struct sk_buff *skb) {
	
	int err = 0;
	
	struct scatterlist *src_sg = (struct scatterlist *)
		kzalloc(3 * sizeof(struct scatterlist), GFP_ATOMIC);
	struct scatterlist *dst_sg = src_sg + 1;
	struct scatterlist *aad_sg = dst_sg + 1;
	
	struct mptcp_cb *mpcb = tcp_sk(sk)->mpcb;
	struct sock *meta_sk = mpcb->meta_sk;
	__be64 *data_seq =  (__be64 *) kzalloc(3*sizeof(__u64), GFP_ATOMIC);
	__u64 *data_level_length = data_seq+2;
	u8 *nonce = (u8 *) data_seq;

	struct crypto_aead *tfm = tcp_sk(sk)->mpcb->rcv_tfm;
	struct aead_request *req = NULL;
	struct mptcpsec_decr_callback_args *decryption_arguments = NULL;

	u32 src_len = skb->len - mptcpsec_option_tag_len(sk);
	u32 aad_len = 0;
	
	*data_seq = mptcpsec_get_decrypt_data_seq_64(skb, mpcb);
	/* We don't count the DATA-FIN because it wasn't count in encryption */
	*data_level_length = skb->len;

	if (!tcp_sk(sk)->mpcb->rcv_tfm) {
		mptcp_debug("%s : Structure not yet initialized\n", __func__);
		err = 1;
		goto free_structures;
	} else if (!skb->len) {
	        mptcpsec_data_decr_entail_queue(meta_sk, sk, skb);
		goto free_structures;
	}

        req = aead_request_alloc(tfm, GFP_ATOMIC);
	if (IS_ERR(req)) {
		err = PTR_ERR(req);
		goto free_structures;
	}

	/* Allocation */
	if (!skb_shinfo(skb)->nr_frags) {
	        src_sg = (struct scatterlist *) kzalloc(2 * sizeof(struct scatterlist), GFP_ATOMIC);
		dst_sg = src_sg;
		aad_sg = dst_sg + 1;
		sg_init_one(src_sg, skb->data, src_len);
	} else {
		
		int start_frag = -1;
		int end_frag = -1;
		int start_frag_offset = -1;
		int end_frag_offset = -1;
		int frag_number = get_number_fragments(skb, 0, src_len, &start_frag, &end_frag,
						       &start_frag_offset, &end_frag_offset);
		src_sg = (struct scatterlist *) kzalloc((frag_number + 1) * sizeof(struct scatterlist), GFP_ATOMIC);
		set_sg_table_from_skb_frags(skb, start_frag, end_frag, start_frag_offset, end_frag_offset, src_sg);
		dst_sg = src_sg;
        	aad_sg = dst_sg + frag_number;
	}
	sg_init_one(aad_sg, data_level_length, aad_len);

	/* Request fill-in */
	decryption_arguments = (struct mptcpsec_decr_callback_args *)
		kzalloc(sizeof(struct mptcpsec_decr_callback_args), GFP_ATOMIC);
	if (!decryption_arguments) {
		err = 1;
		goto free_structures;
	}
	decryption_arguments->subsk = sk;
	decryption_arguments->skb = skb;
	decryption_arguments->data_seq = data_seq;
	decryption_arguments->scatter_sg = src_sg;
	
	aead_request_set_callback(req, CRYPTO_TFM_REQ_MAY_BACKLOG, mptcpsec_data_decr_callback,
				  (void *) decryption_arguments);
	aead_request_set_crypt(req, src_sg, dst_sg, src_len, nonce);
	aead_request_set_assoc(req, NULL, aad_len);

	/* Launch aead & wait for completion */
        err = crypto_aead_decrypt(req);
	if (err == -EINPROGRESS || err == -EBUSY) {
		return err;
	} else if (err == -EBADMSG) {
		mptcp_debug("%s : The decryption authentication has failed\n", __func__);
		mptcpsec_reject_skb(sk, skb);
		goto free_structures;
	} else if (err < 0) {
	        mptcp_debug("%s : The decryption has failed to start with error code %d\n", __func__, err);
		mptcpsec_reject_skb(sk, skb);
		goto free_structures;
	} else {
		/* Decryption is finished => change the queue of the skb */
	        mptcpsec_data_decr_finished(meta_sk, sk, skb);
	}

free_structures:
	/* Free structures */
	if (req)
		aead_request_free(req);
	if (data_seq)
		kfree(data_seq); /* Free also the data_level_length */
	if (src_sg)
		kfree(src_sg); /* Free also the dst_sg and aad_sg */
	if (decryption_arguments)
		kfree(decryption_arguments);
	return err;
}

/* Authentication related functions */

static int mptcpsec_option_auth_finished(struct sock *sk, struct sk_buff *skb,
					 int tcp_header_size) {

	struct inet_sock *inet = inet_sk(sk);
	struct tcp_sock *tp = tcp_sk(sk);
	const struct inet_connection_sock *icsk = inet_csk(sk);
	struct tcp_skb_cb *tcb = TCP_SKB_CB(skb);
	int err = 0;

	if (skb->ip_summed != CHECKSUM_PARTIAL) {
		skb->csum = csum_partial(((u8 *) skb->data) + tcp_header_size, skb->len - tcp_header_size, 0);
	}

	icsk->icsk_af_ops->send_check(sk, skb);

	tcp_event_pkt_sent(sk, skb, tcp_header_size);

	if (after(tcb->end_seq, tp->snd_nxt) || tcb->seq == tcb->end_seq)
		TCP_ADD_STATS(sock_net(sk), TCP_MIB_OUTSEGS,
			      tcp_skb_pcount(skb));

	/* OK, its time to fill skb_shinfo(skb)->gso_segs */
	skb_shinfo(skb)->gso_segs = tcp_skb_pcount(skb);

	/* Our usage of tstamp should remain private */
	skb->tstamp.tv64 = 0;

	/* Cleanup our debris for IP stacks */
	memset(skb->cb, 0, max(sizeof(struct inet_skb_parm),
			       sizeof(struct inet6_skb_parm)));
	
        err = icsk->icsk_af_ops->queue_xmit(sk, skb, &inet->cork.fl);

	if (likely(err <= 0))
		return err;

	tcp_enter_cwr(sk);

	return net_xmit_eval(err);
}

void mptcpsec_option_auth_callback(struct crypto_async_request *req, int err) {

	struct mptcpsec_auth_callback_args *args =
		(struct mptcpsec_auth_callback_args *) req->data;
	struct sk_buff *skb = args->skb;
	struct sock *subsk = args->subsk;
	struct sock *meta_sk = mptcp_meta_sk(subsk);
	void *mandatory_plaintext = args->mandatory_plaintext;
	u64 *data_seq = args->data_seq;
	struct scatterlist *scatter_sg = args->scatter_sg;
	int tcp_header_size = args->tcp_header_size;

	/* The request was backlogged */
	if (err == -EINPROGRESS)
		return;

	if (err < 0) {
		mptcp_debug("%s : The option authentication failed (with error code %d) for the skb : skb->seq %u, skb->end_seq %u\n", __func__, err, TCP_SKB_CB(skb)->seq, TCP_SKB_CB(skb)->end_seq);
		goto free_skb;
	}

	bh_lock_sock(meta_sk);

	mptcpsec_option_auth_finished(subsk, skb, tcp_header_size);

	bh_unlock_sock(meta_sk);

free_structures:
	/* Free structures */
        kfree(mandatory_plaintext);
	kfree(data_seq); /* Free also the data_level_length */
	kfree(scatter_sg); /* Free also the dst_sg and aad_sg */
	kfree(args);
        aead_request_free((struct aead_request *) req);
	return;
free_skb:
	kfree(skb);
	goto free_structures;
}

int mptcpsec_option_auth(struct sock *sk, struct sk_buff *skb, struct tcp_out_options *opts,
			 unsigned int tcp_payload_size) {

        int err = 0;
	u8 aad_length = 0;
	
	struct scatterlist *src_sg = (struct scatterlist *)
		kzalloc(4 * sizeof(struct scatterlist), GFP_ATOMIC);
	struct scatterlist *dst_sg = src_sg + 1;
	struct scatterlist *aad_sg = dst_sg + 1;
	void *option_tag = NULL;
	void *mandatory_plaintext = NULL;
	
	struct mptcp_cb *mpcb = tcp_sk(sk)->mpcb;
	/* 16-byte nonce */
	__be64 *data_seq =  (__be64 *) kzalloc(2*sizeof(__u64), GFP_ATOMIC);
	u8 *nonce = (u8 *) data_seq;

	struct crypto_aead *tfm = tcp_sk(sk)->mpcb->send_tfm;
	struct aead_request *req = NULL;
	struct mptcpsec_auth_callback_args *authentication_arguments = NULL;

	int mptcp_dss_len = MPTCP_SUB_LEN_DSS_ALIGN + MPTCP_SUB_LEN_ACK_ALIGN +
		MPTCP_SUB_LEN_SEQ_ALIGN;
	
	*data_seq = mptcpsec_get_auth_data_seq_64(skb, mpcb);

        /* dss is in a union with inet_skb_parm and
	 * the IP layer expects zeroed IPCB fields.
	 */
	memset(TCP_SKB_CB(skb)->dss, 0, mptcp_dss_len);

	/* TFM structure should either be already initialized
	 * or we are in the ACK of the 3-way handshake
	 */
	if (!tfm) {
		err = mptcpsec_option_auth_finished(sk, skb, skb->len - tcp_payload_size);
		goto free_structures;
	}

	if (!opts->authenticated_option_length) {
	        err = mptcpsec_option_auth_finished(sk, skb, skb->len - tcp_payload_size);
		goto free_structures;
	}

	// REMOVE WHEN PURE ACK CASES ARE HANDLED ON RECEIVING SIDE
        if (!tcp_payload_size) {
	        err = mptcpsec_option_auth_finished(sk, skb, skb->len - tcp_payload_size);
		goto free_structures;
	}
	// REMOVE WHEN PURE ACK CASES ARE HANDLED ON RECEIVING SIDE

        req = aead_request_alloc(tfm, GFP_ATOMIC);
	if (IS_ERR(req)) {
	        err = PTR_ERR(req);
		goto free_structures;
	}

	/* Allocation */
	if (tcp_payload_size) {
		option_tag = skb->data + skb->len - mptcpsec_option_tag_len(sk);
	} else {
		/* Pure ACK case */
		option_tag = skb_put(skb, mptcpsec_option_tag_len(sk));
	}
	mandatory_plaintext = kzalloc(mptcpsec_aead_tag_len(sk), GFP_ATOMIC);
	
	sg_init_one(src_sg, mandatory_plaintext, mptcpsec_aead_tag_len(sk));
	sg_init_one(dst_sg, option_tag, mptcpsec_option_tag_len(sk));
	sg_init_one(aad_sg, opts->authenticated_options, opts->authenticated_option_length);
	aad_length = opts->authenticated_option_length;

	/* Request fill-in */
	authentication_arguments = (struct mptcpsec_auth_callback_args *)
		kzalloc(sizeof(struct mptcpsec_auth_callback_args), GFP_ATOMIC);
	if (!authentication_arguments) {
		err = 1;
		goto free_structures;
	}
	authentication_arguments->subsk = sk;
	authentication_arguments->skb = skb;
	authentication_arguments->mandatory_plaintext = mandatory_plaintext;
        authentication_arguments->data_seq = data_seq;
        authentication_arguments->scatter_sg = src_sg;
	authentication_arguments->tcp_header_size = skb->len - tcp_payload_size;
	
	aead_request_set_callback(req, CRYPTO_TFM_REQ_MAY_BACKLOG, mptcpsec_option_auth_callback, (void *) authentication_arguments);
	aead_request_set_crypt(req, src_sg, dst_sg, mptcpsec_aead_tag_len(sk), nonce);
	aead_request_set_assoc(req, aad_sg, aad_length);

	/* Launch aead & wait for completion */
        err = crypto_aead_encrypt(req);
	if (err == -EINPROGRESS || err == -EBUSY) {
		return 0;
	} else if (err < 0) {
		mptcp_debug("%s : The option authentication has failed to start with error code %d\n", __func__, err);
	} else {
		/* Authentication is finished => Send the semgent to the IP layer */
	        err = mptcpsec_option_auth_finished(sk, skb, authentication_arguments->tcp_header_size);
	}

free_structures:
	/* Free structures */
	if (req)
		aead_request_free(req);
	if (mandatory_plaintext)
		kfree(mandatory_plaintext);
	if (data_seq)
		kfree(data_seq); /* Free also the data_level_length */
	if (src_sg)
		kfree(src_sg); /* Free also the dst_sg and aad_sg */
	if (authentication_arguments)
		kfree(authentication_arguments);
	return err;
}

/* Authentication check related functions */

static int mptcpsec_option_check_auth_finished(struct sock *sk, struct sk_buff *skb) {

	return mptcpsec_data_decr(sk, skb);
}

void mptcpsec_option_check_auth_callback(struct crypto_async_request *req, int err) {

	struct mptcpsec_check_auth_callback_args *args =
		(struct mptcpsec_check_auth_callback_args *) req->data;
	struct sk_buff *skb = args->skb;
	struct sock *subsk = args->subsk;
	struct sock *meta_sk = mptcp_meta_sk(subsk);
	void *mandatory_plaintext = args->mandatory_plaintext;
	u64 *data_seq = args->data_seq;
	struct scatterlist *scatter_sg = args->scatter_sg;

	/* The request was backlogged */
	if (err == -EINPROGRESS)
		return;

	if (err == -EBADMSG) {
		mptcp_debug("%s : Options raised authentication error\n", __func__);
	        mptcpsec_reject_skb_lock(subsk, skb);
		goto free_structures;
	} else if (err) {
		mptcp_debug("%s : The option check failed (with error code %d) for the skb : skb->seq %u, skb->end_seq %u\n", __func__, err, TCP_SKB_CB(skb)->seq, TCP_SKB_CB(skb)->end_seq);
	        mptcpsec_reject_skb_lock(subsk, skb);
		goto free_structures;
	}

	bh_lock_sock(meta_sk);

	mptcpsec_option_check_auth_finished(subsk, skb);

	bh_unlock_sock(meta_sk);

free_structures:
	/* Free structures */
        kfree(mandatory_plaintext);
	kfree(data_seq); /* Free also the data_level_length */
	kfree(scatter_sg); /* Free also the dst_sg and aad_sg */
	kfree(args);
        aead_request_free((struct aead_request *) req);
}

static u8 mptcpsec_parse_mptcp_authentified_options(const unsigned char *ptr, int opsize,
						    struct sk_buff *skb) {

	const struct mptcp_option *mp_opt = (struct mptcp_option *)ptr;
	u8 authenticated_option_length = 0;

        if (mp_opt->sub == MPTCP_SUB_DSS) {
		authenticated_option_length += opsize;
	}

	return authenticated_option_length;
}

static u8 mptcpsec_parse_authentified_options(struct sk_buff *skb,
					      void **authenticated_option) {

	u8 authenticated_option_length = 0;
	unsigned char *ptr;
	struct tcphdr *th = tcp_hdr(skb);
	int length = (th->doff * 4) - sizeof(struct tcphdr);

	ptr = (unsigned char *)(th + 1);

	while (length > 0) {
		int opcode = *ptr++;
		int opsize;

		switch (opcode) {
		case TCPOPT_EOL:
			return authenticated_option_length;
		case TCPOPT_NOP:	/* Ref: RFC 793 section 3.1 */
			length--;
			continue;
		default:
			opsize = *ptr++;
			if (opcode == TCPOPT_MPTCP) {
				authenticated_option_length +=
					mptcpsec_parse_mptcp_authentified_options(ptr - 2,
										  opsize, skb);
				if (authenticated_option_length) {
					*authenticated_option = ptr - 2;
				}
			}
			ptr += opsize-2;
			length -= opsize;
		}
	}

	return authenticated_option_length;
}

int mptcpsec_data_check_auth_all(struct sock *meta_sk) {

	struct tcp_sock *meta_tp = tcp_sk(meta_sk);
	struct sk_buff *skb_tmp = NULL;
	struct sk_buff *tmp = NULL;
	struct sock *subsk = NULL;
	int err = 0;
	int old_err = 0;

	skb_queue_walk_safe(&meta_tp->mpcb->mptcpsec_to_decrypt_queue, skb_tmp, tmp) {
		subsk = skb_tmp->sk;
		skb_orphan(skb_tmp);
		skb_set_owner_r(skb_tmp, meta_sk);
		err = mptcpsec_option_check_auth(subsk, skb_tmp);
		old_err = !old_err && err ? err : old_err;
	}

	skb_queue_walk_safe(&meta_tp->mpcb->mptcpsec_to_decrypt_out_of_order_queue, skb_tmp, tmp) {
		subsk = skb_tmp->sk;
		skb_orphan(skb_tmp);
		skb_set_owner_r(skb_tmp, meta_sk);
		err = mptcpsec_option_check_auth(subsk, skb_tmp);
		old_err = !old_err && err ? err : old_err;
	}

	return old_err;
}

int mptcpsec_option_check_auth(struct sock *sk, struct sk_buff *skb) {

	int err = 0;
	
	struct scatterlist *src_sg = NULL;
	struct scatterlist *dst_sg = NULL;
	struct scatterlist *aad_sg = NULL;
	void *mandatory_plaintext = NULL;
	void *authenticated_options = NULL;
	u8 authenticated_option_length = 0;
	u8 aad_length = 0;
	
	struct mptcp_cb *mpcb = tcp_sk(sk)->mpcb;
	__be64 *data_seq =  (__be64 *) kzalloc(2*sizeof(__u64), GFP_ATOMIC); // 16 bytes of nonce
	u8 *nonce = (u8 *) data_seq;

	struct crypto_aead *tfm = tcp_sk(sk)->mpcb->rcv_tfm;
	struct aead_request *req = NULL;
	struct mptcpsec_check_auth_callback_args *checker_arguments = NULL;
	
	*data_seq = mptcpsec_get_decrypt_data_seq_64(skb, mpcb);

	/* TFM structure not yet initialized */
	if (!tfm) {
		err = 1;
		/* We reput the sock as the owner of the sock
		 * since authentication will be launched when tfm is initialized
		 */
		skb_orphan(skb);
		skb_set_owner_r(skb, sk);
		goto free_structures;
	}

	if (!skb->len && mptcp_is_data_fin(skb)) {
		//REMOVE WHEN PURE ACKS ARE HANDLED
	        err = mptcpsec_option_check_auth_finished(sk, skb);
		goto free_structures;
		//REMOVE WHEN PURE ACKS ARE HANDLED
	}

	if (skb->len != mptcpsec_option_tag_len(sk)
	    && skb->len <= mptcpsec_tags_space(sk)) {
		/* Not an data-free segment
		 * and not enough space for both tags
		 */
		mptcp_debug("%s : Size of the payload not applicable to MPTCPsec\n", __func__);
		mptcp_send_reset(sk);
	        err = PTR_ERR(req);
		goto free_structures;
	}

        req = aead_request_alloc(tfm, GFP_ATOMIC);
	if (IS_ERR(req)) {
	        err = PTR_ERR(req);
		goto free_structures;
	}

	/* Allocation */

	if (!skb_shinfo(skb)->nr_frags) {
		u8 *option_tag = skb->data + skb->len - mptcpsec_option_tag_len(sk);
		src_sg = (struct scatterlist *) kzalloc(3 * sizeof(struct scatterlist), GFP_ATOMIC);
		sg_init_one(src_sg, option_tag, mptcpsec_option_tag_len(sk));
		dst_sg = src_sg + 1;
        	aad_sg = dst_sg + 1;
	} else {
		int start_frag = -1;
		int end_frag = -1;
		int start_frag_offset = -1;
		int end_frag_offset = -1;
		int frag_number = get_number_fragments(skb, skb->len - mptcpsec_option_tag_len(sk), skb->len - 1,
						       &start_frag, &end_frag, &start_frag_offset,
						       &end_frag_offset);
		src_sg = (struct scatterlist *) kzalloc((frag_number + 2) * sizeof(struct scatterlist), GFP_ATOMIC);
		set_sg_table_from_skb_frags(skb, start_frag, end_frag, start_frag_offset, end_frag_offset, src_sg);
		dst_sg = src_sg + frag_number;
        	aad_sg = dst_sg + 1;
	}
        
	mandatory_plaintext = kzalloc(mptcpsec_option_tag_len(sk), GFP_ATOMIC);
	sg_init_one(dst_sg, mandatory_plaintext, mptcpsec_option_tag_len(sk));
	
	authenticated_option_length = mptcpsec_parse_authentified_options(skb, &authenticated_options);
	/* This case shouldn't happen because the presence of the DSS is checked before */
	BUG_ON(!authenticated_option_length);
	sg_init_one(aad_sg, authenticated_options, authenticated_option_length);
	aad_length = authenticated_option_length;

	/* Request fill-in */
	checker_arguments = (struct mptcpsec_check_auth_callback_args *)
		kzalloc(sizeof(struct mptcpsec_check_auth_callback_args), GFP_ATOMIC);
	if (!checker_arguments) {
		err = 1;
		goto free_structures;
	}
	checker_arguments->subsk = sk;
	checker_arguments->skb = skb;
	checker_arguments->mandatory_plaintext = mandatory_plaintext;
	checker_arguments->data_seq = data_seq;
	checker_arguments->scatter_sg = src_sg;
	
	aead_request_set_callback(req, CRYPTO_TFM_REQ_MAY_BACKLOG, mptcpsec_option_check_auth_callback, (void *) checker_arguments);
	aead_request_set_crypt(req, src_sg, dst_sg, mptcpsec_option_tag_len(sk), nonce);
	aead_request_set_assoc(req, aad_sg, aad_length);

	/* Launch aead */
        err = crypto_aead_decrypt(req);
	if (err == -EINPROGRESS || err == -EBUSY) {
		return 0;
	} else if (err == -EBADMSG) {
		mptcp_debug("%s : Options raised authentication error\n", __func__);
	        mptcpsec_reject_skb(sk, skb);
	} else if (err < 0) {
		mptcp_debug("%s : The option check has failed to start with error code %d\n", __func__, err);
	        mptcpsec_reject_skb(sk, skb);
	} else {
		/* Authentication is finished => Send the semgent to the IP layer */
	        err = mptcpsec_option_check_auth_finished(sk, skb);
	}

free_structures:
	/* Free structures */
	if (req)
		aead_request_free(req);
	if (mandatory_plaintext)
		kfree(mandatory_plaintext);
	if (data_seq)
		kfree(data_seq); /* Free also the data_level_length */
	if (src_sg)
		kfree(src_sg); /* Free also the dst_sg and aad_sg */
	if (checker_arguments)
		kfree(checker_arguments);
	return err;
}
